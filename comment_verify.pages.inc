<?php

/**
 * @file
 * Page callbacks for the Comment Verify module.
 */

function comment_verify_comment_verify_form($form, &$form_state, $comment) {
  $node = node_load($comment->nid);
  $form['comment_preview'] = array(
    '#markup' => comment_view($comment, $node, 'preview'),
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['publish'] = array(
    '#type' => 'submit',
    '#title' => t('Publish comment'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#title' => t('Delete comment'),
  );
}