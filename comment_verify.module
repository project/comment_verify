<?php

/**
Hello [comment:author],

You recently posted a comment on [comment:node] at [site:name]. To publish your comment, please visit
[comment:url-verify]. Until you click this link and publish your comment, it will not be visible on
the site.
*/

/**
 * Implements hook_menu().
 */
function comment_verify_menu() {
  $items['comment/%comment/verify/%'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('comment_verify_comment_verify_form', 1, 3),
    'access callback' => 'comment_verify_comment_verify_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
    'file' => 'comment_verify.pages.inc',
  );

  return $items;
}

/**
 * Generate a token based on $value and private key.
 *
 * @param $comment
 *   An additional value to base the token on.
 */
function comment_verify_get_token($comment) {
  $data = array(
    $comment->cid,
    $comment->created,
    $comment->mail,
    'verify'
  );
  return drupal_hmac_base64(implode(':', $data), drupal_get_private_key() . drupal_get_hash_salt());
}

function comment_verify_comment_verify_access($comment, $token, $account = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }
  return empty($account->uid) && empty($comment->uid) && empty($comment->status) && $token === comment_verify_get_token($comment) && node_access('view', node_load($comment->nid));
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function comment_verify_form_node_type_form_alter(&$form, &$form_state) {
  $form['comment']['comment_verify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require anonymous commenters to verify their comments via email.'),
    '#default_value' => variable_get('comment_verify_' . $form['#node_type']->type, 1),
    '#access' => user_access('post comments', drupal_anonymous_user()),
    '#states' => array(
      'visible' => array(
        'comment_anonymous_' . $form['#node_type']->type => array('value' => COMMENT_ANONYMOUS_MUST_CONTACT),
      ),
    ),
  );
}

/**
 * Implements hook_comment_insert().
 */
function comment_verify_comment_insert($comment) {

}

/**
 * Implements hook_comment_presave().
 */
function comment_verify_comment_presave($comment) {
  if (empty($comment->cid) && empty($comment->uid) && !empty($comment->mail)) {
    // Does this email address have an existing comment that has been approved?
    $comment->verified = (bool) db_query("SELECT 1 FROM {comment} WHERE mail = :mail AND status = :status", array(':mail' => $comment->mail, ':status' => COMMENT_PUBLISHED))->fetchField();
    $comment->status = $comment->verified && user_access('skip comment approval') ? COMMENT_PUBLISHED : COMMENT_NOT_PUBLISHED;
  }
}

function comment_verify_can_verify($comment) {
  return empty($comment->uid) && !empty($comment->mail) && $comment->status == COMMENT_NOT_PUBLISHED;
}

function comment_verify_process($comment) {
  comment_verify_publish($comment);

  // Approve any existing comments by this user as well.
  /*if ($cids = db_query("SELECT cid FROM {comment} WHERE cid <> :cid AND mail = :mail AND status = :status", array(':cid' => $comment->cid, ':mail' => $comment->mail, ':status' => COMMENT_NOT_PUBLISHED))) {
    $comments = comment_load_multiple($cids);
    array_walk($comments, 'comment_mail_verify_publish');
  }*/
}

function comment_verify_publish($comment) {
  $comment->status = COMMENT_PUBLISHED;
  comment_save($comment);
  module_invoke_all('comment_publish', $comment);
}

function comment_verify_get_verify_url($comment) {
  if (comment_verify_can_verify($comment)) {
    return 'comment/' . $comment->cid . '/verify/' . comment_verify_get_token($comment);
  }
}