<?php

/**
 * @file
 * Token integration for the Comment Verify module.
 */

/**
 * Implements hook_token_info().
 */
function commment_verify_token_info() {
  $info['tokens']['comment']['verify-url'] = array(
  	'name' => t('Verification URL'),
  	'description' => t('The URL to verify the email for an unpublished, anonymous comment.'),
  	'type' => 'url',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function comment_verify_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'comment' && !empty($data['comment'])) {
    $comment = $data['comment'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'verify-url':
          if ($verify_url = comment_verify_get_verify_url($comment)) {
            $replacements[$original] = url($verify_url, $url_options);
          }
          break;
      }
    }

    // [comment:mail-verify-url:*] chained token replacements.
    if (($verify_url_tokens = token_find_with_prefix($tokens, 'verify-url')) && $verify_url = comment_verify_get_verify_url($comment)) {
      $replacements += token_generate('url', $verify_url_tokens, array('path' => $verify_url), $options);
    }
  }

  return $replacements;
}